from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.engine.base import Engine
from sqlalchemy.engine.url import URL
from sqlalchemy.ext.declarative import declarative_base


DeclarativeBase = declarative_base()

def create_items_table(engine: Engine):
    """
    Create the Items table
    """
    DeclarativeBase.metadata.create_all(engine)


class Films(DeclarativeBase):
    """
    Defines the Films model
    Sqlalchemy deals model
    """

    __tablename__ = "film"

    id = Column("id", Integer, primary_key=True)
    title = Column("title", String)
    episode_id = Column("episode_id", Integer)
    opening_crawl = Column("opening_crawl", String)
    director = Column("director", String)
    producer= Column("producer", String)
    character_count = Column("character_count", Integer)
    planet_count = Column("planet_count", Integer)
    starship_count = Column("starship_count", Integer)
    vehicle_count = Column("vehicle_count", Integer)
    species_count = Column("species_count", Integer)
    release_date = Column("release_date", String)
    created = Column("created", String)
    edited = Column("edited", String)

#
class People(DeclarativeBase):
    """
    Defines the People model
    Sqlalchemy deals model
    """

    __tablename__ = "people"

    id = Column("id", Integer, primary_key=True)
    name = Column("name", String)
    height =  Column("height", Integer)
    mass = Column("mass", Integer)
    hair_color = Column("hair_color", String)
    skin_color= Column("skin_color", String)
    eye_color = Column("eye_color", String)
    birth_year = Column("birth_year", String)
    gender = Column("gender", String)
    homeworld = Column("homeworld", String)
    species_count = Column("species_count", Integer)
    vehicle_count = Column("vehicle_count", Integer)
    starship_count = Column("starship_count", Integer)
    created =  Column("created", String)
    edited =  Column("edited", String)

class Film_People(DeclarativeBase):
    """
    Defines the Film_People model
    Sqlalchemy deals model
    """

    __tablename__ = "Film_People"

    filmid = Column("filmid", Integer, primary_key=True)
    peopleid = Column("peopleid", Integer)
