import scrapy
import json
from ..items import SWfilm

class FilmSpider(scrapy.Spider):
    name = 'film'
    allowed_domains = ['swapi.dev']
    start_urls = ['https://swapi.dev/api/films/?format=json']

    def parse(self, response):
        film = SWfilm()
        jsonresponse = json.loads(response.text)
        for elem in jsonresponse["results"]:
            id = int([s for s in elem["url"].split("/") if s][-1])
            title = elem["title"]
            episode_id = int(elem["episode_id"])
            opening_crawl = elem["opening_crawl"].replace('\r\n',' ')
            director = elem["director"]
            producer = elem["producer"]
            character_count = len(elem["characters"])
            planet_count = len(elem["planets"])
            starship_count = len(elem["starships"])
            vehicle_count = len(elem["vehicles"])
            species_count = len(elem["species"])
            release_date =  elem["release_date"]
            created = elem["created"]
            edited =  elem["edited"]

            # item setting
            film["id"] = id
            film["title"] = title
            film["episode_id"] = episode_id
            film["opening_crawl"] = opening_crawl
            film["director"] = director
            film["producer"] = producer
            film["character_count"] = character_count
            film["planet_count"] = planet_count
            film["starship_count"] = starship_count
            film["vehicle_count"] = vehicle_count
            film["species_count"] = species_count
            film["release_date"] = release_date
            film["created"] = created
            film["edited"] = edited

            yield film

        next_page = jsonresponse["next"]

        if next_page is not None:
            try:
                yield scrapy.Request(
                    next_page, self.parse)
            except ValueError as e:
                self.log(e)
