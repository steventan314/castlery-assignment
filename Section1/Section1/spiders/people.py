import scrapy
import json
import requests
from ..items import SWpeople



class PeopleSpider(scrapy.Spider):
    name = 'people'
    allowed_domains = ['swapi.dev']
    start_urls = ['https://swapi.dev/api/people/?format=json']

    def parse(self, response):
        people = SWpeople()
        jsonresponse = json.loads(response.text)
        for elem in jsonresponse["results"]:
            id = int([s for s in elem["url"].split("/") if s][-1])
            name = elem["name"]
            height = int(float(elem["height"].replace(",",""))) if elem["height"] != "unknown" else 0
            mass = int(float(elem["mass"].replace(",",""))) if elem["mass"] != "unknown" else 0
            hair_color = elem["hair_color"]
            skin_color = elem["skin_color"]
            eye_color = elem["eye_color"]
            birth_year = elem["birth_year"]
            gender = elem["gender"]

            homeworldURL = elem["homeworld"]
            homeworld = requests.get(homeworldURL).text

            species_count = len(elem["species"])
            vehicle_count = len(elem["vehicles"])
            starship_count = len(elem["starships"])
            created = elem["created"]
            edited =  elem["edited"]
            filmsID = [int([s for s in filmURL.split("/") if s][-1]) for filmURL in elem["films"]]


            # item setting
            people["id"] = id
            people["name"] = name
            people["height"] = height
            people["mass"] = mass
            people["hair_color"] = hair_color
            people["skin_color"] = skin_color
            people["eye_color"] = eye_color
            people["birth_year"] = birth_year
            people["gender"] = gender
            people["homeworld"] = homeworld
            people["species_count"] = species_count
            people["vehicle_count"] = vehicle_count
            people["starship_count"] = starship_count
            people["created"] = created
            people["edited"] = edited
            people["filmsID"] = filmsID

            yield people

        next_page = jsonresponse["next"]

        if next_page is not None:
            try:
                yield scrapy.Request(
                    next_page, self.parse
                )
            except ValueError as e:
                self.log(e)
