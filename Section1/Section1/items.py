# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy

class SWfilm(scrapy.Item):
    id = scrapy.Field()
    title = scrapy.Field()
    episode_id = scrapy.Field()
    opening_crawl = scrapy.Field()
    director = scrapy.Field()
    producer= scrapy.Field()
    character_count = scrapy.Field()
    planet_count = scrapy.Field()
    starship_count = scrapy.Field()
    vehicle_count = scrapy.Field()
    species_count = scrapy.Field()
    release_date = scrapy.Field()
    created = scrapy.Field()
    edited = scrapy.Field()

class SWpeople(scrapy.Item):
    id = scrapy.Field()
    name = scrapy.Field()
    height = scrapy.Field()
    mass = scrapy.Field()
    hair_color = scrapy.Field()
    skin_color= scrapy.Field()
    eye_color = scrapy.Field()
    birth_year = scrapy.Field()
    gender = scrapy.Field()
    homeworld = scrapy.Field()
    species_count = scrapy.Field()
    vehicle_count = scrapy.Field()
    starship_count = scrapy.Field()
    created = scrapy.Field()
    edited = scrapy.Field()
    filmsID = scrapy.Field()

