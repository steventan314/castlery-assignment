# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
import logging
import pandas as pd


import psycopg2
from sqlalchemy import create_engine
import io

from .items import SWpeople, SWfilm

DATABASE_PORT = ""
DATABASE_HOST = ""
DATABASE_USER = ""
DATABASE_PASSWORD = ""
DATABASE_NAME = ""


class Section1Pipeline:
    def __init__(self):
        """
        Initializes database connection and sessionmaker.
        Creates items table.
        """


        # Create placeholders for the items
        self.film_list = []
        self.people_list = []



    def process_item(self, item, spider):
        """
        Process the item and store to database.
        """
        if isinstance(item, SWfilm):
            self.film_list.append(dict(**item))
        elif isinstance(item, SWpeople):
            self.people_list.append(dict(**item))
        else:
            logging.ERROR("ERROR: Item class not reognised")
        return item

    def close_spider(self, spider):
        # Add items to Postgres Database
        film_df = pd.DataFrame(self.film_list)
        people_df = pd.DataFrame(self.people_list)


        # If there are people scraped
        if people_df.shape[0] > 0:
            # Convert created and edited times from string to datetime
            people_df["created"] = pd.to_datetime(people_df["created"])
            people_df["edited"] = pd.to_datetime(people_df["edited"])


        #     Extract out the film people list from the DF

            film_people_list = []
            for i, filmlist in enumerate(people_df["filmsID"]):
                for film in filmlist:
                    new_people_film = [film, people_df["id"][i]]
                    film_people_list.append(new_people_film)
            #
            film_people_map = pd.DataFrame(film_people_list, columns=['film_id', 'people_id'])

        #     Remove film id from the people_df
            people_df.drop("filmsID", inplace=True, axis=1)

            # Upload df to DB
            engine = create_engine(
                f'postgresql+psycopg2://{DATABASE_USER}:{DATABASE_PASSWORD}@localhost:{DATABASE_PORT}/{DATABASE_NAME}')

            # drops old table and creates new empty table

            people_df.head(0).to_sql('dim_people', engine, if_exists='replace',index=False)

            conn = engine.raw_connection()
            cur = conn.cursor()
            output = io.StringIO()
            people_df.to_csv(output, sep='\t', header=False, index=False)
            output.seek(0)
            contents = output.getvalue()
            cur.copy_from(output, 'dim_people', null="")  # null values become ''
            conn.commit()


            film_people_map.head(0).to_sql('film_people_map', engine, if_exists='replace',index=False)

            conn = engine.raw_connection()
            cur = conn.cursor()
            output = io.StringIO()
            film_people_map.to_csv(output, sep='\t', header=False, index=False)
            output.seek(0)
            contents = output.getvalue()
            cur.copy_from(output, 'film_people_map', null="")  # null values become ''
            conn.commit()
            engine.connect().execute("ALTER TABLE dim_people ADD PRIMARY KEY (id)")
            engine.connect().execute("ALTER TABLE film_people_map ADD PRIMARY KEY (film_id,people_id)")


        if film_df.shape[0] > 0:
            # Convert created and edited times from string to datetime
            film_df["created"] = pd.to_datetime(film_df["created"])
            film_df["edited"] = pd.to_datetime(film_df["edited"])
            film_df["release_date"] = pd.to_datetime(film_df["release_date"])

            # Upload df to DB
            engine = create_engine(
                f'postgresql+psycopg2://{DATABASE_USER}:{DATABASE_PASSWORD}@localhost:{DATABASE_PORT}/{DATABASE_NAME}')

            # drops old table and creates new empty table
            film_df.head(0).to_sql('dim_film', engine, if_exists='replace',index=False)

            conn = engine.raw_connection()
            cur = conn.cursor()
            output = io.StringIO()
            film_df.to_csv(output, sep='\t', header=False, index=False)
            output.seek(0)
            contents = output.getvalue()
            cur.copy_from(output, 'dim_film', null="")  # null values become ''
            conn.commit()

            engine.connect().execute("ALTER TABLE dim_film ADD PRIMARY KEY (id)")