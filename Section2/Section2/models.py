from sqlalchemy import Column, Integer, String, Float, DateTime, create_engine
from sqlalchemy.engine.base import Engine
from sqlalchemy.engine.url import URL
from sqlalchemy.ext.declarative import declarative_base


DeclarativeBase = declarative_base()

def create_items_table(engine: Engine):
    """
    Create the Items table
    """
    DeclarativeBase.metadata.create_all(engine)


class Movies(DeclarativeBase):
    """
    Defines the Films model
    Sqlalchemy deals model
    """

    __tablename__ = "movie"
    year = Column("id", Integer)
    title = Column("id", String)
    reviews = Column("id", Integer)
    tomatometer = Column("id", Float)
    audience_score = Column("id", Float)
    synopsis= Column("id", String)
    rating = Column("id", String)
    genre = Column("id", String)
    original_language = Column("id", String)
    director = Column("id", String)
    producer = Column("id", String)
    writer = Column("id", String)
    release_date_theaters = Column("id", String)
    release_date_streaming = Column("id", String)
    runtime_mins = Column("id", Integer)
    distributor = Column("id", String)
    production_co = Column("id", String)
    aspect_ratio = Column("id", String)
    url = Column("id", String)

