import scrapy
from ..items import movie

class RottentomatoSpider(scrapy.Spider):
    name = 'rottentomato'
    allowed_domains = ['www.rottentomatoes.com']
    start_urls = ['https://www.rottentomatoes.com/top/bestofrt/?year=2020']

    def parse(self, response):
        RTmovie = movie()
        SINGLE_ITEM_CSS = "table.table > tr"

        domain = "https://www.rottentomatoes.com"
        for elem in response.css(SINGLE_ITEM_CSS):
            year = response.url.split("year=")[1]
            title = elem.css("td a.unstyled.articleLink::text").get().strip()
            reviews = elem.css("td.right.hidden-xs::text").get()
            url = domain + elem.css("td a.unstyled.articleLink::attr(href)").get().strip()

            # Add information to RTmovie
            RTmovie["year"] = year
            RTmovie["title"] =title
            RTmovie["reviews"] = reviews
            RTmovie["url"]= url

            movie_req = scrapy.Request(url, callback=self.parse_movie_detail)
            movie_req.meta["RTmovie"] = RTmovie.copy()


            yield movie_req


    def parse_movie_detail(self, response):
        RTmovie = response.meta["RTmovie"]

        tomatometer = float(response.css("score-board::attr(tomatometerscore)").get())/100 if response.css("score-board::attr(tomatometerscore)").get() != "" else 0.0
        audience_score = float(response.css("score-board::attr(audiencescore)").get())/100 if response.css("score-board::attr(audiencescore)").get() != "" else 0.0
        synopsis = response.css("div.movie_synopsis::text").get().strip()

        rating = response.xpath("//li[@class='meta-row clearfix' and div/text()='Rating:']/div[@class='meta-value']/text()").get()
        genre_raw = response.xpath("//li[@class='meta-row clearfix' and div/text()='Genre:']/div[@class='meta-value genre']/text()").get()
        if genre_raw is not None:
            genre=','.join([genre.strip() for genre in genre_raw.split('\n') if genre.strip() is not ''])
        else:
            genre=''

        original_language = response.xpath("//li[@class='meta-row clearfix' and div/text()='Original Language:']/div[@class='meta-value']/text()").get()
        director =",".join(response.xpath("//li[@class='meta-row clearfix' and div/text()='Director:']/div[@class='meta-value']//a[@data-qa='movie-info-director']/text()").getall())
        producer = ",".join(response.xpath("//li[@class='meta-row clearfix' and div/text()='Producer:']/div[@class='meta-value']//a/text()").getall())
        writer= ",".join(response.xpath("//li[@class='meta-row clearfix' and div/text()='Writer:']/div[@class='meta-value']//a/text()").getall())

        release_date_theaters =response.xpath("//li[@class='meta-row clearfix' and div/text()='Release Date (Theaters):']/div[@class='meta-value']//time/text()").get()
        release_date_streaming =response.xpath("//li[@class='meta-row clearfix' and div/text()='Release Date (Streaming):']/div[@class='meta-value']//time/text()").get()

        runtime_mins_raw = response.xpath("//li[@class='meta-row clearfix' and div/text()='Runtime:']/div[@class='meta-value']//time/text()").get()
        runtime_mins = 0
        if runtime_mins_raw is not None:
            runtime_mins_raw = runtime_mins_raw.strip().lower().split(' ')
            for time in runtime_mins_raw:
                if "h" in time:
                    runtime_mins += int(time.replace("h",''))*60
                if "m" in time:
                    runtime_mins += int(time.replace("m",''))


        distributor = response.xpath("//li[@class='meta-row clearfix' and div/text()='Distributor:']/div[@class='meta-value']/text()").get()
        production_co = response.xpath("//li[@class='meta-row clearfix' and div/text()='Sound Mix:']/div[@class='meta-value']/text()").get()
        aspect_ratio = response.xpath("//li[@class='meta-row clearfix' and div/text()='Aspect Ratio:']/div[@class='meta-value']/text()").get()

        RTmovie["tomatometer"] = tomatometer
        RTmovie["audience_score"] = audience_score
        RTmovie["synopsis"] = synopsis.strip() if synopsis is not None else None
        RTmovie["rating"] = rating.strip() if rating is not None else None
        RTmovie["genre"] = genre
        RTmovie["original_language"] = original_language.strip() if original_language is not None else None
        RTmovie["director"] = director.strip() if director is not None else None
        RTmovie["producer"] = producer.strip() if producer is not None else None
        RTmovie["writer"] = writer.strip() if writer is not None else None
        RTmovie["release_date_theaters"] = release_date_theaters.strip() if release_date_theaters is not None else None
        RTmovie["release_date_streaming"] = release_date_streaming.strip() if release_date_streaming is not None else None
        RTmovie["runtime_mins"] = runtime_mins
        RTmovie["distributor"] = distributor.strip() if distributor is not None else None
        RTmovie["production_co"] = production_co.strip() if production_co is not None else None
        RTmovie["aspect_ratio"] = aspect_ratio.strip() if aspect_ratio is not None else None

        yield RTmovie


