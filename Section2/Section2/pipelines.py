# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
import logging
import pandas as pd


import psycopg2
from sqlalchemy import create_engine
import io

DATABASE_PORT = ""
DATABASE_HOST = ""
DATABASE_USER = ""
DATABASE_PASSWORD = ""
DATABASE_NAME = ""


class Section1Pipeline:
    def __init__(self):
        """
        Initializes database connection and sessionmaker.
        Creates items table.
        """


        # Create placeholders for the items
        self.movie = []



    def process_item(self, item, spider):
        """
        Process the item and store to database.
        """
        self.movie.append(dict(**item))
        return item

    def close_spider(self, spider):
        # Add items to Postgres Database
        movie_df = pd.DataFrame(self.movie)

        # If there are people scraped
        if movie_df.shape[0] > 0:
            movie_df["release_date_theaters"] = pd.to_datetime(movie_df["release_date_theaters"])
            movie_df["release_date_streaming"] = pd.to_datetime(movie_df["release_date_streaming"])

            # Upload df to DB
            engine = create_engine(
                f'postgresql+psycopg2://{DATABASE_USER}:{DATABASE_PASSWORD}@localhost:{DATABASE_PORT}/{DATABASE_NAME}')

            # drops old table and creates new empty table
            movie_df.head(0).to_sql('fact_top_movies', engine, if_exists='replace',index=False)

            conn = engine.raw_connection()
            cur = conn.cursor()
            output = io.StringIO()
            movie_df.to_csv(output, sep='\t', header=False, index=False)
            output.seek(0)
            contents = output.getvalue()
            cur.copy_from(output, 'fact_top_movies', null="")  # null values become ''
            conn.commit()
