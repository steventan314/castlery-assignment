# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


import scrapy

class movie(scrapy.Item):
    year = scrapy.Field()
    title = scrapy.Field()
    reviews = scrapy.Field()
    tomatometer = scrapy.Field()
    audience_score = scrapy.Field()
    synopsis= scrapy.Field()
    rating = scrapy.Field()
    genre = scrapy.Field()
    original_language = scrapy.Field()
    director = scrapy.Field()
    producer = scrapy.Field()
    writer = scrapy.Field()
    release_date_theaters = scrapy.Field()
    release_date_streaming = scrapy.Field()
    runtime_mins = scrapy.Field()
    distributor = scrapy.Field()
    production_co = scrapy.Field()
    aspect_ratio = scrapy.Field()
    url = scrapy.Field()
