from flask import Flask, jsonify
from flask_restful import Api, Resource, reqparse
import pandas as pd
import json

# We create a Flask app
app = Flask(__name__)
api = Api(app)

data_arg=reqparse.RequestParser()
data_arg.add_argument("id" , type=int ,help="Enter id")
data_arg.add_argument("employee" , type=str ,help="Enter Name")
data_arg.add_argument("gender" , type=str ,help="Enter Gender")
data_arg.add_argument("age" , type=int ,help="Enter Age")
data_arg.add_argument("salary" , type=int ,help="Enter Salary")
data_arg.add_argument("town" , type=str ,help="Enter Town")

# id_arg=reqparse.RequestParser()
# id_arg.add_argument("id" , type=int ,help="Enter id")

# We establish a Flask route so that we can serve HTTP traffic on that route
@app.route('/')

class Read_Add(Resource):
    def __init__(self):
        # read csv file
        self.data = pd.read_csv('employees.csv')
    def get(self):
        all_employee_json = self.data.to_json(orient='records')
        return jsonify({'message': all_employee_json})

    # POST request on the url will hit this function
    def post(self):
        # data parser to parse data from url
        args = data_arg.parse_args()
        # if id is already present
        if((self.data['id']==args.id).any()):
            return jsonify({"message": 'id already exist'})
        else:
            # Save data to csv
            self.data= self.data.append(args, ignore_index=True)
            self.data.to_csv("employees.csv", index=False)
            return jsonify({"message": 'Done'})


class Read_Update_Delete(Resource):
    def __init__(self):
        # read csv file
        self.data = pd.read_csv('employees.csv')
    def get(self):
        # data parser to parse data from url
        args = data_arg.parse_args()

        employee_csv = self.data.loc[self.data['id']==args.id]
        employee_json = employee_csv.to_json(orient='records')

        return jsonify({'message': employee_json})

    # POST request on the url will hit this function
    def post(self):
        args = data_arg.parse_args()
        if ((self.data['id'] == args.id).any()):
            # if id already present Update it
            self.data=self.data.drop(self.data["id"].loc[self.data["id"] == args.id].index)
            self.data = self.data.append(args, ignore_index=True)
            self.data.to_csv("employees.csv", index=False)
            return jsonify({"message": 'Updated successfully'})
        else:
            # If id not present Do no update
            return jsonify({"message": 'id does not exist'})
        # PUT request on the url will hit this function

    def delete(self):
        args = data_arg.parse_args()
        if ((self.data['id'] == args.id).any()):
            # id it present delete data from csv
            self.data = self.data.drop(self.data["id"].loc[self.data["id"] == args.id].index)
            print(self.data)
            self.data.to_csv("employees.csv", index=False)
            return jsonify({"message": 'Deleted successfully'})
        else:
            return jsonify({"message": 'Not Present'})

# Add URL endpoints
api.add_resource(Read_Add, '/employee')
api.add_resource(Read_Update_Delete,'/employee/id')

if __name__ == '__main__':
    app.run(debug=True)